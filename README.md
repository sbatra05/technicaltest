This solution focus on deploying a NODE JS application into a Dockerfile

  - The application has a single endpoint "/status".
  - Docker image runs on ports 9000:4000
  - http://localhost:9000/status URL returns output in the JSON format containing Application Version, Last Commit SHA and Description.

# Files in this Solution

  - app.js
  - Dockerfile
  - package.json
  - .gitlab-ci.yml